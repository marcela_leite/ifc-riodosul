<?php
class Aluno{
	var $nome;
	var $idade;
	var $matricula;
	var $presente;
	public function __construct($n,$i,$p){
		$this->nome = $n;
		$this->idade = $i;
		$this->presente = $p;
		$this->matricula = substr($n,0,1).mt_rand(1000,9999);
	}
	public function __destruct(){
		//echo "Destruindo o objeto ".get_class($this);
	}
	public function imprimeAluno(){
		echo "<pre>";
		var_dump($this);
		echo "</pre>";
	}

	public function responderChamada(){
		if ($this->presente)
			echo $this->nome.": presente querida professora!<br>";
		else
			echo " ausente<br>";
	}

}

?>