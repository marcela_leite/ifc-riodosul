<?php
class Professor{
	var $nome;
	var $cargaHoraria;
	public function __construct($n,$ch){
		$this->nome = $n;
		$this->cargaHoraria = $ch;
	}
	public function __destruct(){
		//echo "Destruindo o objeto ".get_class($this);
	}
	public function imprimeProfessor(){
		echo "<pre>";
		var_dump($this);
		echo "</pre>";
	}

	public function fazerChamada($lista){
		echo "Bom dia queridos alunos!<br>";
		echo "Vou fazer a chamada. Prestem atenção!<br>";
		foreach ($lista as $aluno) {
			echo $aluno->nome.">";
			$aluno->responderChamada();
		}
	}



}
?>