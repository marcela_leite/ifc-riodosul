<?php
	include "Aluno.class.php";
	include "Professor.class.php";	
	$a1 = new Aluno("Maria",13,true);
	$a2 = new Aluno("Mariana",15,true);
	$a3 = new Aluno("Marta",35,false);
	$a4 = new Aluno("Margarida",25,true);
	$a5 = new Aluno("Marcia",33,false);
	$a6 = new Aluno("Marieta",11,false);
	$a7 = new Aluno("Marica",78,true);
	
	$listaAlunos = [$a1,$a2,$a3,$a4,$a5,$a6,$a7];

	$prof = new Professor("Marcela",40);
	$prof->fazerChamada($listaAlunos);
?>