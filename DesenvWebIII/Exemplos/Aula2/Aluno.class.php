<?php
	class Aluno{
		var $nome;
		var $idade;
		var $matricula;
		public function __construct($n,$i){
			echo "Criando o objeto ".get_class();
			$this->nome = $n;
			$this->idade = $i;
			$this->matricula = substr($n,0,1).mt_rand(1000,9999);
		}
		public function __destruct(){
			echo "Destruindo o objeto ".get_class($this);
		}
		public function imprimeAluno(){
			echo "<pre>";
			var_dump($this);
			echo "</pre>";
		}
	}
?>